#include "mainwindow.h"
#include <QApplication>

// Adapted from http://stackoverflow.com/a/11202102/1014522
void bdoMessageHandler(QtMsgType type, const QMessageLogContext &, const QString & msg)
{
    QString txt;

    switch (type) {
        case QtDebugMsg:
            txt = QString("Debug: %1").arg(msg);
            break;
        case QtInfoMsg:
            txt = QString("Info: %1").arg(msg);
            break;
        case QtWarningMsg:
            txt = QString("Warning: %1").arg(msg);
            break;
        case QtCriticalMsg:
            txt = QString("Critical: %1").arg(msg);
            break;
        case QtFatalMsg:
            txt = QString("Fatal: %1. Aborting.").arg(msg);
            break;
    }

    QFile outFile("bdo.log");
    outFile.open(QIODevice::WriteOnly | QIODevice::Append);
    QTextStream ts(&outFile);
    ts << txt << endl;

    if (type == QtFatalMsg) {
        abort();
    }
}

int main(int argc, char *argv[])
{
    qInstallMessageHandler(bdoMessageHandler);

    QDateTime now = QDateTime::currentDateTime();
    qInfo() << " === Starting at " << now.toString() << "===";

    qDebug() << "SSL version for build: " << QSslSocket::sslLibraryBuildVersionString();
    qDebug() << "SSL version for run-time: " << QSslSocket::sslLibraryVersionNumber();
    qDebug() << QCoreApplication::libraryPaths();

    QApplication a(argc, argv);

    QTranslator qtTranslator;
    qtTranslator.load("qt_" + QLocale::system().name(),
                      QLibraryInfo::location(QLibraryInfo::TranslationsPath));
    a.installTranslator(&qtTranslator);

    QTranslator bdoTranslator;
    QString locale = QLocale::system().name(); // fr_FR, en_US, ...
    locale.truncate(locale.lastIndexOf('_')); // fr, en, ...

    if (locale != "en") {
        QString translation = "translations/bdo_" + locale;
        if(!bdoTranslator.load(translation)) {
            qWarning() << "could not load " << translation << ".qm";
        }
        a.installTranslator(&bdoTranslator);
    }

    MainWindow w;
    w.show();

    return a.exec();
}
