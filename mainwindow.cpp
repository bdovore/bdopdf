/*
 *  This file is part of BdoPdf.
 *
 *  Copyright 2015, bdovore.com
 *
 *  BdoPdf is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, version 2.1 of the License.
 *
 *  BdoPdf is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with BdoPdf. If not, see <http://www.gnu.org/licenses/>.
 */

#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    setupMenuBar();

    // Creates a subfolder named "couv" in directory where the executable is run
    // NB: mkpath() checks if the path exists and creates the folder tree if it
    //     doesn't exist (i.e. mkpath("./foo/bar/couv") creates foo and foo/bar)
    QDir().mkpath("couv");

    ui->statusBar->showMessage(tr("Go to File") + " --> " + tr("Import XML"));
    setCentralWidget(ui->scrollArea);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::setupMenuBar()
{
    // FILE
    QMenu * fileMenu = menuBar()->addMenu(tr("&File"));

    QAction * action = fileMenu->addAction(tr("Import XML"));
    connect(action, SIGNAL(triggered()), this, SLOT(loadXML()));
    action = fileMenu->addAction(tr("Download covers"));
    connect(action, SIGNAL(triggered()), this, SLOT(downloadCovers_clicked()));
    action = fileMenu->addAction(tr("&Quit"));
    connect(action, SIGNAL(triggered()), this, SLOT(close()));

    // Collection browser
    QMenu * collMenu = menuBar()->addMenu(tr("&Collection"));
    action = collMenu->addAction(tr("Browse"));
    connect(action, SIGNAL(triggered()), this, SLOT(browseCollection_clicked()));

    // PDF
    QMenu * pdfMenu = menuBar()->addMenu(tr("&PDF"));
    action = pdfMenu->addAction(tr("Create PDF"));
    connect(action, SIGNAL(triggered()), this, SLOT(createPdf_clicked()));

    pdfMenu->addSeparator();

    // PDF Options
    action = pdfMenu->addAction(tr("Choose PDF Font"));
    connect(action, SIGNAL(triggered()), this, SLOT(setPdfFont()));

    action = pdfMenu->addAction(tr("Split PDF by Series"));
    action->setCheckable(true);
    action->setChecked(false);
    action->setStatusTip(tr("Start each serie on a new page."));
    connect(action, SIGNAL(toggled(bool)), this, SLOT(setPdfSplitBySerie(bool)));

    // HELP
    QMenu * helpMenu = menuBar()->addMenu(tr("&Help"));
    action = helpMenu->addAction(tr("About"));
    connect(action, SIGNAL(triggered()), this, SLOT(about()));
}

void MainWindow::loadXML()
{
    xmlFileName = QFileDialog::getOpenFileName(this,tr("Open XML File"), QDir::homePath(), tr("XML Files (*.xml)"));

    if ( xmlFileName.isNull() ) {
        // debug
        ui->statusBar->showMessage(tr("XML import failed, no filename."));
        return;
    }

    QFile file(xmlFileName);
    //try to open
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        QMessageBox::critical(this,
                              "MainWindow::loadXML",
                              "Couldn't open " + xmlFileName,
                              QMessageBox::Ok);
        return;
    }

    // set the filename for the PDF based on the XML filename
    QFileInfo fi(xmlFileName);
    this->pdfFileName = fi.baseName() + ".pdf";

    // TODO check whether a copy happens or not
    this->collection = Bdo::Xml::load(&file);

    // debug
    QString status = tr("XML file imported: ");
    status += QString::number(this->collection.size()) + " series.";
    ui->statusBar->showMessage(status);

//    // debug
//    QList<Bdo::Serie>::const_iterator it = this->collection.constBegin();
//    while (it != this->collection.constEnd()) {
//        QTextStream(stdout) << it->title << " : " << it->tomes.size() << endl;
//        ++it;
//    }

}

void MainWindow::browseCollection_clicked()
{
    QWidget * mainwidget = new QWidget();
    QVBoxLayout * vLayout = new QVBoxLayout();
    mainwidget->setLayout(vLayout);
    ui->scrollArea->setWidgetResizable(true);
    ui->scrollArea->setWidget(mainwidget);

    if (this->collection.empty()) {
        QLabel * err = new QLabel(tr("Empty collection. Load XML first."));
        vLayout->addWidget(err);
        return;
    }

    QList<Bdo::Serie>::iterator it = this->collection.begin();
    QList<Bdo::Serie>::iterator e = this->collection.end();
    while (it != e) {
        Bdo::QSerie * qserie = new Bdo::QSerie(&(*it)); // or &(*it) ...
        vLayout->addWidget(qserie);

        ++it;
    }
}

void MainWindow::createPdf_clicked()
{
    this->makePdf();
}

void MainWindow::makePdf()
{
    if ( this->collection.empty() )
        return;
    else
        qInfo() << "PDF: Exporting " << this->collection.size() << " series.";

    // getSaveFileName automatically checks whether a file exists or not
    // and presents a confirmation dialog to the user.
    QString fileName = QFileDialog::getSaveFileName(this, tr("Export as ..."),
                               this->pdfFileName,
                               tr("PDF (*.pdf)"));

    // user pressed cancel
    if (fileName.isNull())
        return;

    // TODO erase existing PDF first ? (if any)
    // it is possible that sometimes/on some plateform overwrite doesn't work properly
    // (bug signaled on bdovore.com by TaoNico, not confirmed yet)
    // this freezes the whole app for as long as it takes to finish
    //if (!this->pdf.save(this->collection,fileName))
    //    QTextStream(stdout) << "Error: could not create PDF." << endl;

    // this doesn't freeze the app (and I thought slots were asynchronous?)
    QFuture<bool> future = QtConcurrent::run(&this->pdf,&Bdo::Pdf::save,this->collection,fileName);

    connect(&watcher, SIGNAL(finished()), this, SLOT(when_pdf_finished()));
    watcher.setFuture(future);

    ui->statusBar->showMessage(tr("PDF: exporting ..."));
//    future.waitForFinished();

//    if (!future.result())
//        QTextStream(stdout) << "Error: could not create PDF." << endl;
//    else
//        QTextStream(stdout) << "PDF saved." << endl;
}

void MainWindow::when_pdf_finished()
{
    ui->statusBar->showMessage(tr("PDF: export finished."));
}

void MainWindow::downloadCovers_clicked()
{
    this->downloadCovers();
}

void MainWindow::downloadCovers()
{
    // Download covers
    // TODO put this in an asynchronous call / thread ?
    QList<Bdo::Serie>::const_iterator serie = this->collection.constBegin();
    // TODO proper "Serie" iterator
    QList<Bdo::Tome>::const_iterator tome;

    qInfo() << "Starting download.";

    while (serie != this->collection.constEnd()) {
        qInfo() << "Download of serie " << serie->title;
        tome = serie->tomes.constBegin();
        while (tome != serie->tomes.constEnd()) {
            // download cover if not already there
            // TODO add an option to force download even
            // if the file exists.
            // TODO handle errors (yes, they *do* happen
            // quite often. On a test with 3000 covers,
            // about 100 were not properly downloaded)
            QFile file(tome->couv);
            if (!file.exists()) {
                qInfo() << "Download " << tome->couv << " (for tome " << tome->title << ")";
                fd.download(QUrl(Bdo::UrlImages + tome->couv));
            } else {
                qInfo() << "File " << tome->couv << " (for tome " << tome->title
                        << ") exists already. Skipping.";
            }
            ++tome;
        }
        ++serie;
    }
}

void MainWindow::loadImage()
{

}

void MainWindow::about()
{
    QMessageBox::about(this,tr("About"),"BdoPdf " + Bdo::Version + "\n\n" + tr("Create a PDF file from bdovore.com XML export."));
}

void MainWindow::setPdfFont()
{
    bool ok = false;
    QFont f = QFontDialog::getFont(&ok);

    if(ok)
        this->pdf.setFont(f);
}

void MainWindow::setPdfSplitBySerie(bool b)
{
    this->pdf.setSplitBySerie(b);
}
