#ifndef BDO_AUTHOR_H
#define BDO_AUTHOR_H

#include <QString>

namespace Bdo {

    class Author
    {
     public:
        Author();
        ~Author();

        enum Role {
            Drawing,
            Writing,
            Coloring
        };

        QString nickname;
        QString firstname;
        QString lastname;
    };
}

#endif // BDO_AUTHOR_H
