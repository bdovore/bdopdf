/*
 *  This file is part of BdoPdf.
 *
 *  Copyright 2015, bdovore.com
 *
 *  BdoPdf is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, version 2.1 of the License.
 *
 *  BdoPdf is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with BdoPdf. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BDO_SERIE_H
#define BDO_SERIE_H

#include <QList>
#include <QString>

#include "bdo.h"
#include "tome.h"

namespace Bdo
{
    class Serie
    {
     public:
        Serie();
        ~Serie();

        enum Status {
            Finished = 0,
            Ongoing = 1,
            OneShot = 2,
            Stopped = 3
        };

        // TODO ? pass in private and write getters/setters ?
        QString title; // nom in bd_serie
        QList<Tome> tomes;

        QString category; // LIBELLE in bd_genre
        QString origin; // ORIGINE in bd_genre
        Status status; // FLG_FINI in bd_serie

        inline size_t size() const { return this->tomes.size(); }
        inline bool oneshot() const { return this->status == OneShot; }
        QString statusToString() const;
    };
} // namespace Bdo

#endif // BDO_SERIE_H
