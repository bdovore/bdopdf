/*
 *  This file is part of BdoPdf.
 *
 *  Copyright 2015, bdovore.com
 *
 *  BdoPdf is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, version 2.1 of the License.
 *
 *  BdoPdf is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with BdoPdf. If not, see <http://www.gnu.org/licenses/>.
 */

#include "bdo.h"

namespace Bdo
{
    // OVH now uses Let's Encrypt certificates, so we can use https, however we need to
    // have a fallback plan to http, just in case
    QString Protocol( "https://" );
    QString Domain( "www.bdovore.com" );
    QString Url( Protocol + Domain );
    QString UrlImages( Url + "/images/" );
    QString UrlCouv( UrlImages + "couv/" );
    QString Version( "beta 3" );
}
