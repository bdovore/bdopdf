/*
 *  This file is part of BdoPdf.
 *
 *  Copyright 2015, bdovore.com
 *
 *  BdoPdf is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, version 2.1 of the License.
 *
 *  BdoPdf is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with BdoPdf. If not, see <http://www.gnu.org/licenses/>.
 */

#include "tome.h"

using namespace Bdo;

Tome::Tome()
{
    this->eo = false;
    this->toPurchase = false;
    this->flgInt = false;
    this->flgCof = false;
    this->oneShot = false;
}

Tome::~Tome()
{

}

QString Tome::toString()
{
    QString s(" | ");
    QString t(this->title);

    t += s + this->num;

    QList< QPair<Author,Author::Role> >::const_iterator author = this->authors.constBegin();

    for (; author != this->authors.constEnd(); ++author) {
        t += s + author->first.nickname;
    }
    t += s + this->editor;
    t += s + this->collection;
    t += s + this->publication.toString("yyyy-MM-dd");
    t += s + this->isbn;
    t += s + this->ean;
    t += s + this->couv;

    return t;
}
