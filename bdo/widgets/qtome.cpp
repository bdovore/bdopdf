#include "qtome.h"

namespace Bdo
{

QTome::QTome(Tome * t, QWidget *parent) : QWidget(parent), tome(t)
{
    // does it leak when calling again ?
    this->mainLayout = new QHBoxLayout();
    this->cardLayout = new QVBoxLayout();
    mainLayout->setAlignment(Qt::AlignLeft | Qt::AlignTop);
    cardLayout->setAlignment(Qt::AlignTop);

    cover = new QLabel();

    this->mainLayout->addWidget(cover);

    this->mainLayout->addLayout(cardLayout);
    this->setLayout(this->mainLayout);

    updateTome();
}

QTome::~QTome()
{
    delete cover;
    delete cardLayout;
    delete mainLayout;
}

void QTome::setTome(Tome *t)
{
    tome = t;
    updateTome();
}

void QTome::updateTome()
{
    if (!tome) {
        cover->setText("");
        return;
    }

    double maxImgHeight = 240;
    double maxImgWidth = 240;

    cover->setText(tome->couv);
    QPixmap p(tome->couv);
    //QImage p(tome->couv);

    if (p.isNull()) {
        qWarning() << "pix " << tome->couv << " is null";
        p = QPixmap("default.png");

        if (p.isNull()) {
            qWarning() << "default cover not found";
            p = QPixmap(maxImgWidth,maxImgHeight);
            p.fill(QColor(128,128,128));
        }
    }

    double h = p.height();
    double w = p.width();

    // cover is to tall --> resize
    if (h > maxImgHeight) {
        w *= (maxImgHeight / h);
        h = maxImgHeight;
    }
    // cover is too wide --> resize
    if (w > maxImgWidth) {
        h *= (maxImgWidth / w);
        w = maxImgWidth;
    }

    if (!p.isNull()) {
        cover->setPixmap(p.scaled(w,h,Qt::KeepAspectRatio));
        //cover->setPixmap(QPixmap::fromImage(p.scaled(w,h,Qt::KeepAspectRatio)));
        cover->adjustSize();
    }

    QLabel * title = new QLabel(tome->title);
    cardLayout->addWidget(title);

    QString text;
    QLabel * num = new QLabel(tome->num);
    // put the number above the cover

    if (tome->num.size() > 0) {
        if (tome->flgInt) {
            text = tr("Collection ") + tome->num; // Intégrale
        } else if (tome->flgCof) {
            text = tr("Case ") + tome->num; // Coffret
        } else {
            text = tome->num;
        }
    } else {
        if (tome->oneShot) {
            text = tr("One-Shot");
        } else if (tome->flgInt) {
            text = tr("Collection"); // Intégrale
        } else if (tome->flgCof) {
            text = tr("Case"); // Coffret in French
        } else {
            text = tr("Special edition"); // Hors-série
        }
    }

    num->setText(text);
    cardLayout->addWidget(num);

    QList<QPair<Bdo::Author, Bdo::Author::Role> >::const_iterator author = tome->authors.constBegin();

    while (author != tome->authors.constEnd()) {
        QString auth;

        switch(author->second) {
        case Bdo::Author::Drawing:
            auth = tr("Drawing: ");
            break;
        case Bdo::Author::Writing:
            auth = tr("Scenario: ");
            break;
        case Bdo::Author::Coloring:
            auth = tr("Color: ");
            break;
        default:
            auth = "";
        }

        auth += author->first.nickname;

        QLabel * authLabel = new QLabel(auth);
        cardLayout->addWidget(authLabel);

        ++author;
    }

    QLabel * editor = new QLabel();
    text = tome->editor;
    if (tome->collection.size() > 0 && tome->collection != "<N/A>")
        text += " - " + tome->collection;

    editor->setText(text);
    cardLayout->addWidget(editor);

    if (tome->publication.isValid()) {
        QLabel * pub = new QLabel(tome->publication.toString("yyyy-MM-dd"));
        cardLayout->addWidget(pub);
    }
    if (tome->isbn.size() > 0) {
        QLabel * isbn = new QLabel(tr("ISBN: ") + tome->isbn);
        cardLayout->addWidget(isbn);
    }
    if (tome->ean.size() > 0) {
        QLabel * ean = new QLabel(tr("EAN: ") + tome->ean);
        cardLayout->addWidget(ean);
    }

    if (tome->toPurchase) {
        QGraphicsOpacityEffect * effect = new QGraphicsOpacityEffect(this);
        effect->setOpacity(0.5);
        this->setGraphicsEffect(effect);
    }
}

} // namespace
