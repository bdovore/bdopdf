#ifndef QSERIE_H
#define QSERIE_H

#include <QLabel>
#include <QWidget>
#include <QGridLayout>
#include <QHBoxLayout>
#include <QVBoxLayout>

#include "bdo/widgets/qtome.h"
#include "bdo/serie.h"

#define BDO_QSERIE_DEFAULT_NCOL 3

namespace Bdo
{
    class QSerie : public QWidget
    {
        Q_OBJECT
    public:
        explicit QSerie(Serie * s, QWidget *parent = 0);
        ~QSerie();
        void setSerie(Serie * s);
        void setNumberOfColumns(int n);
        void updateInfo();
        void updateTitle();
        void collapse();
        void expand();

    protected:
        void paintEvent(QPaintEvent *);
        void mouseReleaseEvent(QMouseEvent *e);

    signals:

    public slots:

    private:
        Serie * serie;
        QVBoxLayout * mainLayout;
        QHBoxLayout * titleLayout;
        QGridLayout * gridLayout;
        QLabel * title;
        QLabel * info;
        bool showTomes;
        int ncol;
    };
}
#endif // QSERIE_H
