#ifndef QTOME_H
#define QTOME_H

#include <QWidget>
#include <QtWidgets>

#include "bdo/tome.h"

namespace Bdo
{
    class QTome : public QWidget
    {
        Q_OBJECT
    public:
        explicit QTome(Tome * t, QWidget *parent = 0);
        ~QTome();

        void setTome(Tome * t);
        void updateTome();

    signals:

    public slots:
    private:
        QHBoxLayout * mainLayout;
        QVBoxLayout * cardLayout;
        QLabel * cover;
        Tome * tome;
    };
}

#endif // QTOME_H
