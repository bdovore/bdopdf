#include "qserie.h"

namespace Bdo {

QSerie::QSerie(Serie * s, QWidget *parent)
    : QWidget(parent), serie(s),
      mainLayout(new QVBoxLayout()),
      titleLayout(new QHBoxLayout()),
      gridLayout(new QGridLayout()),
      title(new QLabel("")),
      info(new QLabel("")),
      showTomes(false),
      ncol(BDO_QSERIE_DEFAULT_NCOL)
{
    mainLayout->setAlignment(Qt::AlignLeft | Qt::AlignTop);
    gridLayout->setAlignment(Qt::AlignLeft);
    gridLayout->setHorizontalSpacing(100);

    titleLayout->setSpacing(0);
    titleLayout->addWidget(title);
    titleLayout->addWidget(info);
    // nest the layouts
    // add serie title above
    this->mainLayout->addLayout(titleLayout);
    //this->mainLayout->addWidget(title);
    // add the albums in a n-column grid
    this->mainLayout->addLayout(gridLayout);
    this->setLayout(this->mainLayout);

    updateTitle();
    updateInfo();
}

QSerie::~QSerie()
{
    delete gridLayout;
    delete titleLayout;
    delete mainLayout;
}

void QSerie::paintEvent(QPaintEvent *)
{
    QPainter painter(this);

//    int lineHeight = 24;
//    QRect titleRect(0,8, this->width(), lineHeight);
//    painter.fillRect(titleRect,QColor(0, 18, 4));
}

void QSerie::mouseReleaseEvent(QMouseEvent * e)
{
    QLabel * child = static_cast<QLabel *>(childAt(e->pos()));

    // we want to toggle the visibility of tomes only for the title bar
    if (child != title)
        return;

    this->showTomes = !this->showTomes;

    if (this->showTomes) {
        this->expand();
    } else {
        this->collapse();
    }
}

void QSerie::setSerie(Serie * s)
{
    this->serie = s;
    updateTitle();
}

void QSerie::setNumberOfColumns(int n)
{
    // who wants more than 1024 columns?
    ncol = qMin(qMax(0,n),1024);
}

void QSerie::updateInfo()
{
    if (!serie) {
        info->setText("");
        return;
    }

    QString status = serie->category + " - " + serie->statusToString();
    QString ntomes = QString::number(serie->tomes.size()) + " volume";

    if (serie->tomes.size() > 1)
        ntomes += "s";

    info->setText(status + " <small>(" + ntomes + ")</small>");
    info->setFixedHeight(24);
    info->setStyleSheet("QLabel { text-align: right; padding-right:0.25em; background-color: rgb(143, 18, 4); color : white; }");
}

void QSerie::updateTitle()
{
    if (!serie) {
        title->setText("");
        return;
    }

    title->setText(serie->title);
    title->setFixedHeight(24);
    title->setSizePolicy(QSizePolicy::MinimumExpanding,
                         QSizePolicy::MinimumExpanding);
    title->setStyleSheet("QLabel { padding-left:0.25em; background-color: rgb(143, 18, 4); color : white; font-weight: bold; }");
}

void QSerie::collapse()
{
    // remove all childs safely.
    // code adapted from doc https://doc.qt.io/qt-5/qlayout.html#takeAt
    QLayoutItem *child;
    while ((child = gridLayout->takeAt(0)) != 0) {
        QWidget * w = child->widget();
        gridLayout->removeWidget(w);
        delete w;
        delete child;
    }

    // otherwise the space occupied remains in place
    mainLayout->update();
}

void QSerie::expand()
{
    int row = 0, column = 0;
    int w = 640, h = 400;
    QList<Bdo::Tome>::iterator it = this->serie->tomes.begin();
    QList<Bdo::Tome>::iterator e = this->serie->tomes.end();
    while (it != e) {
        QTome * qtome = new QTome(&(*it));
        qtome->resize(w,h);

        gridLayout->addWidget(qtome,row,column);

        ++it;
        column++;
        column %= this->ncol;
        if (column == 0)
            row++;
    }
}

}
