/*
 *  This file is part of BdoPdf.
 *
 *  Copyright 2015, bdovore.com
 *
 *  BdoPdf is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, version 2.1 of the License.
 *
 *  BdoPdf is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with BdoPdf. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BDO_XML_H
#define BDO_XML_H

#include <QFile>
#include <QList>
#include <QString>
#include <QXmlStreamReader>

#include "bdo.h"
#include "serie.h"
#include "tome.h"

namespace Bdo
{
    class Xml
    {
     public:
        Xml();
        ~Xml();

        // static for now, I currently see no reason to have an instance of Bdo::Xml,
        // since it's just a one-call throw away tool. It might change though.
        static QList<Serie> load(QString xmlFilePath);
        static QList<Serie> load(QFile * xmlFile);
        static void save(QList<Serie> collection, QString filePath);

        static Serie parseSerie(QXmlStreamReader & xml);
        static Tome parseTome(QXmlStreamReader & xml);
        static QPair<Author,Author::Role> parseAuthor(QXmlStreamReader & xml);
    };
} // namespace Bdo
#endif // BDO_XML_H
