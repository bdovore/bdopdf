/*
 *  This file is part of BdoPdf.
 *
 *  Copyright 2015, bdovore.com
 *
 *  BdoPdf is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, version 2.1 of the License.
 *
 *  BdoPdf is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with BdoPdf. If not, see <http://www.gnu.org/licenses/>.
 */

#include "xml.h"
#include <QDebug>

using namespace Bdo;

Xml::Xml()
{

}

Xml::~Xml()
{

}

QList<Serie> Xml::load(QString xmlFilePath)
{
    QFile file(xmlFilePath);
    //try to open
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        qWarning() << "Couldn't open " << xmlFilePath;
        //throw something;
        return QList<Serie>();
    }

    return load(&file);
}


QList<Serie> Xml::load(QFile * xmlFile)
{
    QList<Serie> collection;

    // FIXME should we check file.exists() before ?
    // start parsing the XML
    QXmlStreamReader xml(xmlFile);

    while (!xml.atEnd() && !xml.hasError()) {
        // Read next element.
        QXmlStreamReader::TokenType token = xml.readNext();
        // If token is just StartDocument, we'll go to next.
        if (token == QXmlStreamReader::StartDocument) {
            continue;
        }
        // If token is StartElement, we'll see if we can read it.
        if (token == QXmlStreamReader::StartElement) {
            // If it's named collection, we'll go to the next.
            if (xml.name() == "collection") {
                continue;
            }
            // If it's named <serie>, we'll dig the information from there.
            if (xml.name() == "serie") {
                // This will go until the next </serie>
                collection.append(parseSerie(xml));
            }
        }
    }

    xml.clear();

    // FIXME QList<> --> no copy overhead ???
    return collection;

}

void Xml::save(QList<Serie> collection, QString filePath)
{
    // TODO export collection to XML file
    qInfo() << "save " << collection.size() << " series to " << filePath;
    qInfo() << "NOT implemented yet. NOT saving.";
}

Serie Xml::parseSerie(QXmlStreamReader & xml)
{
    Serie serie;

    if (xml.tokenType() != QXmlStreamReader::StartElement && xml.name() == "serie") {
        return serie;
    }

    xml.readNext();

    // read until </serie>
    while (!(xml.tokenType() == QXmlStreamReader::EndElement && xml.name() == "serie")) {
        if (xml.tokenType() == QXmlStreamReader::StartElement) {
            // serie name
            if (xml.name() == "nom") {
                xml.readNext();
                serie.title = xml.text().toString();
            }
            // each album in a serie is in-between "<tome>...</tome>" tags
            else if (xml.name() == "tome") {
                // this will go until the </tome>
                serie.tomes.append(parseTome(xml));
            }
            else if (xml.name() == "fini") {
                xml.readNext();
                // Serie::Status is an enum defined in serie.h to match
                // exactly the convention in bdovore.com DB.
                serie.status = static_cast<Serie::Status>(xml.text().toInt());
            }
            else if (xml.name() == "genre") {
                xml.readNext();
                // Serie::Status is an enum defined in serie.h to match
                // exactly the convention in bdovore.com DB.
                serie.category = xml.text().toString();
            }
            else if (xml.name() == "orig") {
                xml.readNext();
                // Serie::Status is an enum defined in serie.h to match
                // exactly the convention in bdovore.com DB.
                serie.origin = xml.text().toString();
            }
        }
        // next element
        xml.readNext();
    }

    if (serie.status == Serie::OneShot) {
        QList<Tome>::iterator tome = serie.tomes.begin();
        while (tome != serie.tomes.end()) {
            tome->oneShot = true;
            ++tome;
        }
    }

    // TODO check if Qt makes a copy of the QString and QList inside (or is it *that* smart ?)
    // if there are copies made, then this is suboptimal and needs improvement
    return serie;
}

Tome Xml::parseTome(QXmlStreamReader & xml)
{
    Tome tome;

    if (xml.tokenType() != QXmlStreamReader::StartElement && xml.name() == "tome") {
        return tome;
    }

    xml.readNext();

    // read until </tome>
    while (!(xml.tokenType() == QXmlStreamReader::EndElement && xml.name() == "tome")) {
        if (xml.tokenType() == QXmlStreamReader::StartElement) {
            // tome name
            if (xml.name() == "titre") {
                xml.readNext();
                tome.title = xml.text().toString();
            }
            else if (xml.name() == "num") {
                xml.readNext();
                tome.num = xml.text().toString();
            }
            else if (xml.name() == "scenario") {
                xml.readNext();
                Author author;
                author.nickname = xml.text().toString();
                tome.authors.append(qMakePair(author,Author::Writing));
            }
            else if (xml.name() == "dessin") {
                xml.readNext();
                Author author;
                author.nickname = xml.text().toString();
                tome.authors.append(qMakePair(author,Author::Drawing));
            }
            else if (xml.name() == "couleur") {
                xml.readNext();
                Author author;
                author.nickname = xml.text().toString();
                tome.authors.append(qMakePair(author,Author::Coloring));
            }
            else if (xml.name() == "author") {
                tome.authors.append(parseAuthor(xml));
            }
            else if (xml.name() == "editeur") {
                xml.readNext();
                tome.editor = xml.text().toString();
            }
            else if (xml.name() == "collection") {
                xml.readNext();
                tome.collection = xml.text().toString();
            }
            else if (xml.name() == "parution") {
                xml.readNext();
                tome.publication = QDate::fromString(xml.text().toString(),"yyyy-MM-dd");
            }
            else if (xml.name() == "isbn") {
                xml.readNext();
                tome.isbn = xml.text().toString();
            }
            else if (xml.name() == "ean") {
                xml.readNext();
                tome.ean = xml.text().toString();
            }
            else if (xml.name() == "eo") {
                xml.readNext();
                tome.eo = (xml.text().toString() == "O") ? true : false;
            }
            else if (xml.name() == "couv") {
                xml.readNext();
                tome.couv = "couv/" + xml.text().toString();
            }
            else if (xml.name() == "flg_achat") {
                tome.toPurchase = true;
            }
            else if (xml.name() == "int") {
                tome.flgInt = true;
                tome.flgCof = false;
            }
            else if (xml.name() == "cof") {
                tome.flgCof = true;
                tome.flgInt = false;
            }
        }

        xml.readNext();
    }

    // TODO check if Qt makes a copy of the Q... variables inside (or is it *that* smart ?)
    // if there are copies made, then this is suboptimal and needs improvement
    return tome;
}

QPair<Author,Author::Role> Xml::parseAuthor(QXmlStreamReader & xml)
{
    QPair<Author,Author::Role> author;

    if (xml.tokenType() != QXmlStreamReader::StartElement && xml.name() == "author") {
        return author;
    }

    xml.readNext();

    // read until </author>
    while (!(xml.tokenType() == QXmlStreamReader::EndElement && xml.name() == "author")) {
        if (xml.tokenType() == QXmlStreamReader::StartElement) {
            // author names
            if (xml.name() == "pseudo") {
                xml.readNext();
                author.first.nickname = xml.text().toString();
            }
            else if (xml.name() == "prenom") {
                xml.readNext();
                author.first.firstname = xml.text().toString();
            }
            else if (xml.name() == "nom") {
                xml.readNext();
                author.first.lastname = xml.text().toString();
            }
            else if (xml.name() == "role") {
                xml.readNext();
                QString role = xml.text().toString();

                if (role == "scenario")
                    author.second = Author::Writing;
                if (role == "dessin")
                    author.second = Author::Drawing;
                if (role == "couleur")
                    author.second = Author::Coloring;
            }
        }

        xml.readNext();
    }

    return author;
}
