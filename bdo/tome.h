/*
 *  This file is part of BdoPdf.
 *
 *  Copyright 2015, bdovore.com
 *
 *  BdoPdf is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, version 2.1 of the License.
 *
 *  BdoPdf is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with BdoPdf. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BDO_TOME_H
#define BDO_TOME_H

#include <QDate>
#include <QString>
#include <QUrl>

#include "bdo.h"
#include "author.h"

namespace Bdo
{
    class Tome
    {
     public:
        Tome();
        ~Tome();

        QString toString();
        //TODO toPdf();

        // TODO ? pass all variable in private and write getters/setters ?

        QString title;
        QString num;

        bool eo;
        bool toPurchase;
        bool flgInt; // FLG_INT in bd_tome
        bool flgCof; // FLG_TYPE in bd_tome (1 == coffret, 0 == standard album)

        // should we use a pointer to parent Serie instead ? (beware of circular dependencies)
        bool oneShot;

        // TODO we should build a QMap of authors for the whole Collection
        // and just point to it with role in this QList to avoid duplicates
        QList< QPair<Author,Author::Role> > authors;

        QString editor;
        QString collection; // collection is linked to editor --> do we represent that ? how ?
        QDate publication;

        QString isbn;
        QString ean;

        // filepath to cover
        QString couv;
    };
} // namespace Bdo

#endif // BDO_TOME_H
