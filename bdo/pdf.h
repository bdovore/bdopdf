#ifndef BDO_PDF_H
#define BDO_PDF_H

#include <QFile>
#include <QFont>
#include <QList>
#include <QPainter>
#include <QPrinter>
#include <QTextStream>

#include "serie.h"

namespace Bdo
{
    class Pdf
    { 
    public:
        Pdf();
        ~Pdf();

        bool save(QList<Serie> collection, QString pdfFilePath="collection bdovore.pdf");

        double drawSerie(double y, QList<Bdo::Serie>::const_iterator serie);
        void drawTome(double x, double y, QList<Bdo::Tome>::const_iterator tome);
        void setFont(QFont f) { this->pdfFont = f; }
        void setSplitBySerie(bool value) { this->splitBySerie = value; }

    private:
        QPrinter printer;
        QPainter painter;

        double maxImgHeight;
        double maxImgWidth;
        int maxCol;
        int maxRow;
        double colWidth;
        double rowHeight;
        double lineHeight;
        double serieHeight;

        QPen defaultPen;

        QFont pdfFont;
        QFont serieTitleFont;
        QFont serieTextFont;
        QFont tomeTitleFont;
        QFont tomeTextFont;

        QColor serieCardBgColor;

        bool splitBySerie;
    };
}

#endif // BDO_PDF_H
