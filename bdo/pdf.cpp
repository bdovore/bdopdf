/*
 *  This file is part of BdoPdf.
 *
 *  Copyright 2015-2017, bdovore.com
 *
 *  BdoPdf is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, version 2.1 of the License.
 *
 *  BdoPdf is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with BdoPdf. If not, see <http://www.gnu.org/licenses/>.
 */

#include "pdf.h"
#include <QDebug>

using namespace Bdo;

Pdf::Pdf()
{ 
    this->maxImgHeight = 128;
    this->maxImgWidth = 92;
    this->maxCol = 2;
    this->maxRow = 6;
    this->lineHeight = 24.0;
    this->serieHeight = lineHeight;

    this->serieCardBgColor = QColor(143, 18, 4);

    this->splitBySerie = false;
}

Pdf::~Pdf()
{

}

bool Pdf::save(QList<Serie> collection, QString pdfFilePath)
{
    this->printer.setOutputFormat(QPrinter::PdfFormat);
    this->printer.setOutputFileName(pdfFilePath);
    this->printer.setCreator("BDovore.com - Export PDF");
    this->printer.setPaperSize(QPrinter::A4);
    this->printer.setPageMargins(12, 10, 12, 10, QPrinter::Millimeter);

    this->colWidth = printer.width() / maxCol;
    this->rowHeight = printer.height() / maxRow;

    if (!this->painter.begin(&this->printer))
        return false;

    this->defaultPen = this->painter.pen();

    this->painter.setFont(this->pdfFont);

    this->serieTitleFont = this->painter.font();
    this->serieTextFont = this->painter.font();
    this->tomeTitleFont = this->painter.font();
    this->tomeTextFont = this->painter.font();

    this->serieTitleFont.setPointSize(10);
    this->serieTitleFont.setBold(true);

    this->serieTextFont.setPointSize(8);

    this->tomeTitleFont.setPointSize(10);
    this->tomeTitleFont.setBold(true);

    this->tomeTextFont.setPointSize(8);

    this->painter.setFont(this->tomeTitleFont);

    QList<Bdo::Serie>::const_iterator serie = collection.constBegin();

    // initial position in PDF: top-left, first column
    double y = 0.0;

    while (serie != collection.constEnd()) {
        qInfo() << "Exporting " << serie->title;

        // serie + tome cards together are too high --> new page
        // or the user wants a new page for every serie
        // this shouldn't happen for the first serie
        if (y + rowHeight + serieHeight > printer.height()
            || (this->splitBySerie && collection.constBegin() != serie))
        {
            printer.newPage();
            y = 0.0;
        }

        y = drawSerie(y, serie);

        ++serie;
    }

    painter.end();

    return true;
}

double Pdf::drawSerie(double y, QList<Bdo::Serie>::const_iterator serie)
{
    int col = 1;
    double x = 0.0;

    // serie titles are put in a reddish rectangle with white font
    QRect serieCard(x,y, printer.width()-x*2, lineHeight-2);
    painter.fillRect(serieCard,serieCardBgColor);
    serieCard.setX(serieCard.x() + 6); // + 6 is for horizontal padding of title
    serieCard.setWidth(serieCard.width() - 12); // for padding of category

    // serie title text
    painter.setFont(serieTitleFont);
    painter.setPen(Qt::white);

    painter.drawText(serieCard, Qt::AlignLeft | Qt::AlignVCenter | Qt::TextWordWrap, serie->title);

    // serie category (SF, humor, fantastic, manga, ...)
    painter.setFont(serieTextFont);
    QString serieText = serie->category + " - " + serie->statusToString();
    painter.drawText(serieCard, Qt::AlignRight | Qt::AlignVCenter, serieText);
    painter.setPen(defaultPen);
    y += lineHeight + 2;

    QList<Bdo::Tome>::const_iterator tome = serie->tomes.constBegin();

    while (tome != serie->tomes.constEnd()) {
        // wrap around column
        if (col > maxCol) {
            col = 1;
            y += rowHeight;
        }
        //if the tome takes too much vertical space, go to next page
        if (y + rowHeight > printer.height()) {
            x = 0.0;
            y = 0.0;
            col = 1;
            printer.newPage();
        }

        x = (col-1) * colWidth;

        drawTome(x, y, tome);

        ++col;
        ++tome;
    }


    // We're done with this serie, be prepared for the next one (if any)
    // --> step one row down and shift left to column 1
    y += rowHeight + lineHeight;

    return y;
}

void Pdf::drawTome(double x, double y, QList<Bdo::Tome>::const_iterator tome)
{
    QRect required;

    QImage pix(tome->couv);
    double coverWidth = 0.0;

    if (pix.isNull()) {
        qWarning() << "pix " << tome->couv << " is null";
        pix = QImage("default.png");

        if (pix.isNull()) {
            qWarning() << "default cover not found";
            pix = QImage(maxImgWidth,maxImgHeight,QImage::Format_ARGB32);
            pix.fill(QColor(128,128,128));
        }
    }

    double h = pix.height();
    double w = pix.width();

    // cover is to tall --> resize
    if (h > maxImgHeight) {
        w *= (maxImgHeight / h);
        h = maxImgHeight;
    }
    // cover is too wide --> resize
    if (w > maxImgWidth) {
        h *= (maxImgWidth / w);
        w = maxImgWidth;
    }

    //draw and resize (+lineHeight to have space for the number)
    painter.drawImage(QRect(x,y+lineHeight,w,h),pix);
    coverWidth = w;

    // put the number above the cover
    QString tomeNumText;
    double tomeNumWidth;

    if (tome->num.size() > 0) {
        if (tome->flgInt) {
            tomeNumText = "INT " + tome->num;
        } else if (tome->flgCof) {
            tomeNumText = "COF " + tome->num;
        } else {
            tomeNumText = tome->num;
        }
    } else {
        if (tome->oneShot) {
            tomeNumText = "OS";
        } else if (tome->flgInt) {
            tomeNumText = "INT";
        } else if (tome->flgCof) {
            tomeNumText = "COF";
        } else {
            tomeNumText = "HS";
        }
    }

    // + 12 is for horizontal padding of number
    tomeNumWidth = painter.fontMetrics().boundingRect(tomeNumText).width() + 24;
    if (tomeNumWidth < lineHeight)
        tomeNumWidth = lineHeight;

    QRect tomeNum(x+(coverWidth-tomeNumWidth)/2,y, tomeNumWidth,lineHeight);
    painter.fillRect(tomeNum,Qt::yellow);
    tomeNum.setX(tomeNum.x());
    painter.setFont(tomeTitleFont);
    painter.drawText(tomeNum, Qt::AlignCenter | Qt::AlignVCenter, tomeNumText);

    // infos
    QRect tomeCard(x+coverWidth,y, colWidth-coverWidth, rowHeight-2);
    QPen penCard(Qt::lightGray, 1, Qt::DotLine, Qt::RoundCap, Qt::RoundJoin);
    painter.setPen(penCard);

    painter.setPen(defaultPen);
    tomeCard.setX(tomeCard.x() + 6); // + 6 is for horizontal padding of title
    tomeCard.setY(tomeCard.y() + 3); // center the first line of the title manually
    //painter.drawRect(tomeCard);

    // tome title (may wrap)
    painter.setFont(tomeTitleFont);
    painter.drawText(tomeCard, Qt::AlignLeft | Qt::TextWordWrap, tome->title, &required);
    double tomeTitleHeight = required.height();

    // other info
    tomeCard.setX(tomeCard.x() + 6); // + 6 is for horizontal padding of info
    tomeCard.setY(tomeCard.y() + (tomeTitleHeight + 6));
    painter.setFont(tomeTextFont);
    //painter.drawText(tomeCard, Qt::AlignLeft | Qt::TextWordWrap, tome->editor);
    QString infoText = "";

    QList<QPair<Bdo::Author, Bdo::Author::Role> >::const_iterator author = tome->authors.constBegin();

    while (author != tome->authors.constEnd()) {
        // that's just a quick draft. Ideally they should be ordered as in bdovore.com
        if (author->second == Bdo::Author::Drawing)
            infoText += "Dessinateur : ";
        else if (author->second == Bdo::Author::Writing)
            infoText += "Scénariste : ";
        else if (author->second == Bdo::Author::Coloring)
            infoText += "Coloriste : ";

        infoText += author->first.nickname + "\n";

        ++author;
    }


    infoText += "Éditeur : " + tome->editor;
    if (tome->collection.size() > 0 && tome->collection != "<N/A>")
        infoText += " - " + tome->collection + "\n";
    else
        infoText += "\n";

    if (tome->publication.isValid())
        infoText += "Parution : " + tome->publication.toString("yyyy-MM-dd") + "\n";
    if (tome->isbn.size() > 0)
        infoText += "ISBN : " + tome->isbn + "\n";
    if (tome->ean.size() > 0)
        infoText += "EAN : " + tome->ean + "\n";

    painter.drawText(tomeCard, Qt::AlignLeft | Qt::TextWordWrap, infoText);

    if (tome->toPurchase) {
        QRect purchaseCard(x-1, y-1, colWidth+1, rowHeight+1);
        painter.fillRect(purchaseCard,QColor(255,255,255,128));
    }
}
