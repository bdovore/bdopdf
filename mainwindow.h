/*
 *  This file is part of BdoPdf.
 *
 *  Copyright 2015, bdovore.com
 *
 *  BdoPdf is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, version 2.1 of the License.
 *
 *  BdoPdf is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with BdoPdf. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFileDialog>
#include <QFontDialog>
#include <QMessageBox>
#include <QXmlStreamReader>
#include <QTextStream>
#include <QtConcurrent/QtConcurrent>

#include "bdo/author.h"
#include "bdo/serie.h"
#include "bdo/tome.h"
#include "bdo/pdf.h"
#include "bdo/xml.h"
#include "bdo/widgets/qserie.h"

#include "filedownloader.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void setupMenuBar();

private:
    void downloadCovers();
    void makePdf();

    Ui::MainWindow *ui;

    QString xmlFileName;
    QString pdfFileName;

    QList<Bdo::Serie> collection;
    FileDownloader fd;
    Bdo::Pdf pdf;
    QFutureWatcher<bool> watcher;

public slots:
    void loadXML();

private slots:
    void browseCollection_clicked();
    void createPdf_clicked();
    void when_pdf_finished();
    void downloadCovers_clicked();
    void loadImage();
    void about();

    // pdf config
    void setPdfFont();
    void setPdfSplitBySerie(bool b);
};

#endif // MAINWINDOW_H
