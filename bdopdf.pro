#-------------------------------------------------
#
# Project created by QtCreator 2015-03-04T21:08:33
#
#-------------------------------------------------

QT       += core gui network printsupport concurrent

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = bdopdf
TEMPLATE = app


SOURCES += main.cpp \
    mainwindow.cpp \
    filedownloader.cpp \
    bdo/bdo.cpp \
    bdo/serie.cpp \
    bdo/tome.cpp \
    bdo/xml.cpp \
    bdo/author.cpp \
    bdo/pdf.cpp \
    bdo/widgets/qtome.cpp \
    bdo/widgets/qserie.cpp

HEADERS  += mainwindow.h \
    filedownloader.h \
    bdo/bdo.h \
    bdo/serie.h \
    bdo/tome.h \
    bdo/xml.h \
    bdo/author.h \
    bdo/pdf.h \
    bdo/widgets/qtome.h \
    bdo/widgets/qserie.h

FORMS    += mainwindow.ui

TRANSLATIONS = languages/bdo_fr.ts
