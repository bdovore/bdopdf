// Public domain
// inspired from https://wiki.qt.io/Download_Data_from_URL

#ifndef FILEDOWNLOADER_H
#define FILEDOWNLOADER_H

#include <QFile>
#include <QObject>
#include <QByteArray>
#include <QImage>
//#include <QImageReader>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>

#include "bdo/bdo.h"

class FileDownloader : public QObject
{
 Q_OBJECT
 public:
    explicit FileDownloader(QObject * parent = 0);
    virtual ~FileDownloader();

    void download(QUrl url);

 signals:
    void downloaded();

 private slots:
    void fileDownloaded(QNetworkReply * reply);

 private:
    QNetworkAccessManager nam;
    // we could use this to give a list of pending downloads
    //QList<QNetworkReply *> mCurrentDownloads;
};

#endif // FILEDOWNLOADER_H
