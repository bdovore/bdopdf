<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="fr_FR">
<context>
    <name>Bdo::QTome</name>
    <message>
        <location filename="../bdo/widgets/qtome.cpp" line="70"/>
        <source>One-Shot</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../bdo/widgets/qtome.cpp" line="62"/>
        <source>Collection </source>
        <translation>Intégrale </translation>
    </message>
    <message>
        <location filename="../bdo/widgets/qtome.cpp" line="64"/>
        <source>Case </source>
        <translation>Coffret </translation>
    </message>
    <message>
        <location filename="../bdo/widgets/qtome.cpp" line="72"/>
        <source>Collection</source>
        <translation>Intégrale</translation>
    </message>
    <message>
        <location filename="../bdo/widgets/qtome.cpp" line="74"/>
        <source>Case</source>
        <translation>Coffret</translation>
    </message>
    <message>
        <location filename="../bdo/widgets/qtome.cpp" line="76"/>
        <source>Special edition</source>
        <translation>Hors-série</translation>
    </message>
    <message>
        <location filename="../bdo/widgets/qtome.cpp" line="90"/>
        <source>Drawing: </source>
        <translation>Dessin : </translation>
    </message>
    <message>
        <location filename="../bdo/widgets/qtome.cpp" line="93"/>
        <source>Scenario: </source>
        <translation>Scénario : </translation>
    </message>
    <message>
        <location filename="../bdo/widgets/qtome.cpp" line="96"/>
        <source>Color: </source>
        <translation>Couleur : </translation>
    </message>
    <message>
        <location filename="../bdo/widgets/qtome.cpp" line="123"/>
        <source>ISBN: </source>
        <translation>ISBN : </translation>
    </message>
    <message>
        <location filename="../bdo/widgets/qtome.cpp" line="127"/>
        <source>EAN: </source>
        <translation>EAN : </translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../mainwindow.ui" line="14"/>
        <source>bdovore</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="55"/>
        <source>&amp;File</source>
        <translation>&amp;Fichier</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="43"/>
        <source>Go to File</source>
        <translation>Allez dans Fichier</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="43"/>
        <location filename="../mainwindow.cpp" line="57"/>
        <source>Import XML</source>
        <translatorcomment>le XML ou l&apos;XML ?</translatorcomment>
        <translation>Importer le XML</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="59"/>
        <source>Download covers</source>
        <translation>Télécharger les couvertures</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="61"/>
        <source>&amp;Quit</source>
        <translation>&amp;Quitter</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="65"/>
        <source>&amp;Collection</source>
        <translation>&amp;Collection</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="66"/>
        <source>Browse</source>
        <translation>Parcourir</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="70"/>
        <source>&amp;PDF</source>
        <translation>&amp;PDF</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="71"/>
        <source>Create PDF</source>
        <translation>Créer le PDF</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="77"/>
        <source>Choose PDF Font</source>
        <translation>Choisir la police du PDF</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="80"/>
        <source>Split PDF by Series</source>
        <translation>Diviser le PDF par séries</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="83"/>
        <source>Start each serie on a new page.</source>
        <translation>Commencer chaque série sur une nouvelle page.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="87"/>
        <source>&amp;Help</source>
        <translation>&amp;Aide</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="88"/>
        <location filename="../mainwindow.cpp" line="246"/>
        <source>About</source>
        <translation>À propos</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="98"/>
        <source>XML import failed, no filename.</source>
        <translation>L&apos;import du fichier XML a échoué, pas de nom de fichier.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="120"/>
        <source>XML file imported: </source>
        <translation>Fichier XML importé : </translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="142"/>
        <source>Empty collection. Load XML first.</source>
        <translation>La collection est vide. Chargez un export XML.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="171"/>
        <source>Export as ...</source>
        <translation>Exporter comme ...</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="173"/>
        <source>PDF (*.pdf)</source>
        <translation>PDF (*.pdf)</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="192"/>
        <source>PDF: exporting ...</source>
        <translation>PDF : création en cours ...</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="203"/>
        <source>PDF: export finished.</source>
        <translation>PDF : création finie.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="246"/>
        <source>Create a PDF file from bdovore.com XML export.</source>
        <translation>Créer un fichier PDF à partir d&apos;un export XML de bdovore.com.</translation>
    </message>
</context>
</TS>
