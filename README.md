A little tool written in Qt to convert an XML file, as exported from bdovore.com, into a nice PDF.

The secret evil plan is to make a full desktop collection browser out of it ;-)

# Compilation

Use QtCreator to open bdopdf.pro and build the project.

QtCreator 3.3.1 (with Qt 5.4.1) was used to create this project, so it is the minimal recommended setup.

# Usage

1. Start the program
2. File --> Load XML (XML is loaded in memory)
3. click on "download covers" (sometimes some covers are not downloaded correctly, wait until the first download is over and then click again to start a second one, it will only download the missing covers)
4. click on "make pdf"

# Dependencies

Only Qt so far :-D (dependency to PoDoFo was removed by using a QPrinter and a QPainter to create the PDF)

# Translation

Source code is written in English (just because) and a translation file exists for French. To update the translation file, first run 

```
lupdate bdopdf.pro
```

(possibly with `-no-obsolete` option if needed).

Then open the `languages/bdo_fr.ts` created by lupdate with Qt Linguist editor and translate the various strings.

Finally, either go to "File -> Release" in Qt Linguist (or use `lrelease` in the command-line). A file named `bdo_fr.qm` is created in the `languages` folder. Move it to the build folder of the application, in the `translations` subfolder.

# License

LGPL 2.1
