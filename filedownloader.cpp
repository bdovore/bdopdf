// Public domain
// inspired from https://wiki.qt.io/Download_Data_from_URL
// also from http://doc.qt.io/qt-5/qtnetwork-download-main-cpp.html (that one is BSD)

#include "filedownloader.h"
#include <QDebug>

FileDownloader::FileDownloader(QObject * parent) :
 QObject(parent)
{
    connect( &this->nam,
             SIGNAL (finished(QNetworkReply*)),
             this,
             SLOT (fileDownloaded(QNetworkReply*)) );
}

FileDownloader::~FileDownloader()
{

}

void FileDownloader::download(QUrl url)
{
    qInfo() << "Request download of " << url.toString();

    QNetworkRequest request(url);
    this->nam.get(request);
    //mCurrentDownloads.append(this->nam.get(request));
}

void FileDownloader::fileDownloaded(QNetworkReply * reply)
{
    // FIXME maybe (?) the write-to-file should not happen here.
    //       Instead, the main should be listening to "downloaded" signals
    //       and act accordingly. We should also avoid recomputing filename ...
    
    //mCurrentDownloads.removeAll(reply);

    QUrl url = reply->url();

    if (reply->error()) {
        qInfo() << "Download of " << url.toEncoded().constData()
                << " failed. Reply error: " << reply->errorString();
    } else {
        QByteArray qba = reply->readAll();

        // transform the QByteArray into a QImage based on headers
        QImage img = QImage::fromData(qba);
        //QImageReader qir(qba); // doesn't work see later.

        // if what we downloaded is actually an image, let's save it.
        // (let's try to avoid downloading "somethingfunny.exe")
        if (!img.isNull()) {
        //if(qir.canRead()) { // this should be faster but it always returns 'false' for reasons.
            //remove "http://bdovore.com/images" to get file name ("couv/CV-12345-12345.jpg")
            QString filepath = url.toString().replace(Bdo::UrlImages,"",Qt::CaseInsensitive);

            // NB: Maybe all the following could be replaced by "img.save(filepath);"
            // but there are drawbacks:
            //
            //     1. we cannot detect creation vs. overwriting (ok, this is just debug)
            //     2. save() cannot *write* all formats, whereas writing the QByteArray
            //        to a file is straightforward (we could convert all to JPG, but ... no)
            //     3. save() compresses images. we could detect for each image the
            //        compression rate, at which point it's just more efficient to write
            //        the QByteArray to a QFile ...


            // create new file (or overwrite existing file)
            // the check for existing file happens *before* starting the download
            // the one here is just for debug
            QFile file(filepath);

            //debug
            if(!file.exists()) {
                qInfo() << "creating file " << filepath;
            } else {
                qInfo() << "overwriting file %s " << filepath;
            }
            fflush(stdout);

            if(file.open(QIODevice::ReadWrite)) {
                file.write(qba);
                file.close();

                // emit a signal
                // TODO pass reply->readAll() and have a slot in mainwindow managing the file creation
                emit downloaded();
            }
        } else {
            qWarning() << "Saving " << url.toEncoded().constData() << " failed: not an image.";
        }
    }

    reply->deleteLater();
}

